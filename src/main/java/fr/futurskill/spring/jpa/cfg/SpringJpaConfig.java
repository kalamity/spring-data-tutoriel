package fr.futurskill.spring.jpa.cfg;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@ComponentScan({"fr.futurskill.spring.jpa.bo","fr.futurskill.spring"})
@EnableJpaRepositories(basePackages = "fr.futurskill.spring.jpa.repo")
@EnableTransactionManagement
@ImportResource("classpath:/spring-jpa.xml")
public class SpringJpaConfig {
}
