package fr.futurskill.spring.jpa.bo;


import fr.futurskill.spring.jpa.model.BankAccount;
import fr.futurskill.spring.jpa.model.BankAccountTransaction;
import fr.futurskill.spring.jpa.model.CustomerAccount;

import java.math.BigDecimal;
import java.util.List;

public interface OperationsBancaire {
    void createCustomerAccount(CustomerAccount customerAccount) throws BankException;
    BankAccount addBankAccount(String customerAccountLogin, String bankAccountNumber) throws BankException;
    BankAccountTransaction addTransaction(String bankAccountNumber, BigDecimal amount, String description) throws BankException;
    List<BankAccountTransaction> getTransactions(String bankAccountNumber, int nbTransaction) throws BankException;
}
