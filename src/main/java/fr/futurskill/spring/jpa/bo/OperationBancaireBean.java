package fr.futurskill.spring.jpa.bo;

import fr.futurskill.spring.jpa.model.BankAccount;
import fr.futurskill.spring.jpa.model.BankAccountTransaction;
import fr.futurskill.spring.jpa.model.CustomerAccount;
import fr.futurskill.spring.jpa.repo.BankAccountRepository;
import fr.futurskill.spring.jpa.repo.CustomerAccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component
@Transactional
public class OperationBancaireBean implements OperationsBancaire {

    @Autowired
    private CustomerAccountRepository customerAccountRepository;

    @Autowired
    private BankAccountRepository bankAccountRepository;


    @Override
    public void createCustomerAccount(final CustomerAccount customerAccount) throws BankException {
        // On verifie si l'utilisateur n'existe pas déjà
        if (customerAccountRepository.existsByLogin(customerAccount.getLogin())) {
            throw new BankException("Ce login est deja utilise");
        }

        customerAccountRepository.save(customerAccount);
    }


    @Override
    public BankAccount addBankAccount(String customerAccountLogin, String bankAccountNumber) throws BankException {
        //verifier qu'un compte bancaire n'existe pas sinon Exception
        if (bankAccountRepository.existsByAccountNumber(bankAccountNumber)) {
            throw new BankException("Le compte bancaire existe déjà ");
        }

        //vérifier que le compte bancaire existe
        if (!customerAccountRepository.existsByLogin(customerAccountLogin)) {
            throw new BankException("Le compte bancaire n'existe pas");
        }

        //Je récupère le comte client avec le login
        final CustomerAccount customerAccount = customerAccountRepository.getByLogin(customerAccountLogin);

        //Je créer un compte bancaire
        final BankAccount bankAccount = new BankAccount();
        bankAccount.setAccountNumber(bankAccountNumber);
        bankAccount.setDescription("Compte courrant");

        //Je rajoute le compte bancaire à la liste des comptes bancaires du client
        customerAccount.getAccounts().add(bankAccount);

        //Je sauvegarde en bdd
        customerAccountRepository.save(customerAccount);

        return bankAccount;
    }


    @Override
    public BankAccountTransaction addTransaction(String bankAccountNumber, BigDecimal amount, String description) throws BankException {
        //vérifier que le compte bancaire existe
        if (!bankAccountRepository.existsByAccountNumber(bankAccountNumber)) {
            throw new BankException("Le compte bancaire n'existe pas");
        }
        final BankAccount bankAccount = bankAccountRepository.getByAccountNumber(bankAccountNumber);
        // BankAccount -> Last transaction
        //cas ou il n'y avait pas de transaction / cas ou il y a dejà des transactions
        final BankAccountTransaction lastTransaction = bankAccount.getLastTransaction();
        final BigDecimal lastBalance;
        if (lastTransaction == null) {
            lastBalance = BigDecimal.ZERO;
        } else {
            lastBalance = lastTransaction.getBalance();
        }

        final BankAccountTransaction newTransaction = new BankAccountTransaction();

        //je renseigne la nouvelle transaction
        final BigDecimal newBalance = lastBalance.add(amount);
        newTransaction.setAmount(amount);
        newTransaction.setBalance(newBalance);
        newTransaction.setDescription(description);
        newTransaction.setDate(new Date(System.currentTimeMillis()));

        //Chainage  : Linked list
        // BankAccount -> Last transaction

        // BankAccount -> New transaction -> Last transaction
        bankAccount.setLastTransaction(newTransaction);
        newTransaction.setPreviousTransaction(lastTransaction);

        //Je sauvegarde en bdd
        bankAccountRepository.save(bankAccount);
        return newTransaction;

    }

    @Override
    public List<BankAccountTransaction> getTransactions(String bankAccountNumber, int nbTransaction) throws BankException {
        //Vérification si le compte existe + récupération
        if (!bankAccountRepository.existsByAccountNumber(bankAccountNumber)) {
            throw new BankException("Le compte bancaire n'existe pas");
        }
        final BankAccount bankAccount = bankAccountRepository.getByAccountNumber(bankAccountNumber);

        //Création de la liste
        final List<BankAccountTransaction> transactions = new ArrayList<>();
        long id = 0; //fakeid sans relation avec la base de donnée

        //Ajout des transactions dans la liste

        /*Boucle for classique
        BankAccountTransaction transaction = bankAccount.getLastTransaction();
        for (int i = 0; i < nbTransaction; i++) {
            if (transaction != null) {
                transactions.add(transaction);
                if (transaction.getPreviousTransaction() != null) {
                    transaction = transaction.getPreviousTransaction()
                } else {
                    break;
                }
            }
        }*/

        //Boucle for avec des objets
        for (BankAccountTransaction transaction = bankAccount.getLastTransaction();
             nbTransaction > 0 && transaction != null;
             nbTransaction--, transaction = transaction.getPreviousTransaction() , id --) {

            // Je recopie le contenu de la transaction  : amont, balance, date, description
            final BankAccountTransaction unlinkedTransaction = new BankAccountTransaction();
            //pour des raisons de sécurité je ne retroune pas l'id de la bdd.
            // Permet de comparer 2 transaction avec les même dates, descriptions
            unlinkedTransaction.setId(id);
            unlinkedTransaction.setAmount(transaction.getAmount());
            unlinkedTransaction.setBalance(transaction.getBalance());
            unlinkedTransaction.setDescription(transaction.getDescription());
            unlinkedTransaction.setDate(transaction.getDate());

           /* On ajoute un transaction non liée car on en peut pas ajouter directement la transaction
                    à cause de la listr de chaine
              LAZY : problem d'acces en dehors de cet appel (no session)
              EAGER : je ramene toutes les transactions precedentes

                    */

            transactions.add(unlinkedTransaction);
        }
        return transactions;
    }
}



