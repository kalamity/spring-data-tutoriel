package fr.futurskill.spring.jpa.repo;

import fr.futurskill.spring.jpa.model.CustomerAccount;
import org.springframework.data.repository.CrudRepository;

public interface CustomerAccountRepository extends CrudRepository<CustomerAccount, Long> {
    boolean existsByLogin(String login);
    CustomerAccount getByLogin(String login);
}
