package fr.futurskill.spring.jpa.repo;

import fr.futurskill.spring.jpa.model.BankAccountTransaction;
import org.springframework.data.repository.CrudRepository;

public interface BankAccountTransactionRepository extends CrudRepository<BankAccountTransaction, Long> {
}
