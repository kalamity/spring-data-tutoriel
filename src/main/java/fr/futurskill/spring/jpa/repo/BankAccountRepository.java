package fr.futurskill.spring.jpa.repo;

import fr.futurskill.spring.jpa.model.BankAccount;
import org.springframework.data.repository.CrudRepository;

public interface BankAccountRepository extends CrudRepository<BankAccount, Long> {
    boolean existsByAccountNumber(String accountNumber);
     BankAccount getByAccountNumber(String accountNumber);

}
