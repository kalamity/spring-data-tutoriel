package fr.futurskill.spring.jpa;

import fr.futurskill.spring.jpa.bo.BankException;
import fr.futurskill.spring.jpa.bo.OperationsBancaire;
import fr.futurskill.spring.jpa.cfg.SpringJpaConfig;
import fr.futurskill.spring.jpa.model.BankAccountTransaction;
import fr.futurskill.spring.jpa.model.CustomerAccount;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.stereotype.Component;


import java.math.BigDecimal;
import java.util.List;

@SpringBootApplication
@Import(SpringJpaConfig.class)
public class Main {

    //User Account
    private static final String FIRST_NAME = "Ada";
    private static final String LAST_NAME = "Lovelace";
    private static final String LOGIN = "alovelace";
    private static final String INVALID_LOGIN = "cnorris";
    private static final String BANK_ACCOUNT_NUMBER = "456458-4386-N-1618";

    //Transactions
    private static final BigDecimal AMOUNT = BigDecimal.valueOf(-50);
    private static final String DESCRIPTION = "Achat chez Prallus";

    private static final BigDecimal AMOUNT2 = BigDecimal.valueOf(500);
    private static final String DESCRIPTION2 = "Salaire";

    private static final BigDecimal AMOUNT3 = BigDecimal.valueOf(-30);
    private static final String DESCRIPTION3 = "Ciné";


    @Autowired
    private OperationsBancaire operationsBancaire;


    public void useCase() throws BankException {
        // 1- Creation d'un compte client
        createCustomerAccount();

        // 2- Ajout d'un compte bancaire
        createBankAccount();

        // 3- Ajout de transactions
        addTransactions();

        // 4 - Affichage de la liste des transaction
        getTransactions();
    }

    private void createCustomerAccount() throws BankException {
        final CustomerAccount customerAccount = new CustomerAccount();

        customerAccount.setFirstName(FIRST_NAME);
        customerAccount.setLastName(LAST_NAME);
        customerAccount.setLogin(LOGIN);

        operationsBancaire.createCustomerAccount(customerAccount);
    }

    private void createBankAccount() throws BankException {
        operationsBancaire.addBankAccount(LOGIN, BANK_ACCOUNT_NUMBER);

    }

    private void addTransactions() throws BankException {
        //On ajoute des transaction sur un compte bancaire qui marche
        operationsBancaire.addTransaction(BANK_ACCOUNT_NUMBER, AMOUNT, DESCRIPTION);

        //on test pls transactions
        operationsBancaire.addTransaction(BANK_ACCOUNT_NUMBER, AMOUNT2, DESCRIPTION2);
        operationsBancaire.addTransaction(BANK_ACCOUNT_NUMBER, AMOUNT3, DESCRIPTION3);

    }

    private void getTransactions() throws BankException {
        final List<BankAccountTransaction> transactions = operationsBancaire.getTransactions(BANK_ACCOUNT_NUMBER, 2);
        if (transactions != null) {
            for (BankAccountTransaction transaction : transactions) {
                System.out.println(transaction);
            }
        }

    }

    /*@Autowired
    public static void main( String[] args) throws BankException {
        // Je dois obtenir un pointeur sur le contexte Spring.
        // La configuration est faite par annotation.
        //Donc j'utilise AnnotationConfigApplicationContext
        final ApplicationContext context = new AnnotationConfigApplicationContext(SpringJpaConfig.class);
        final Main main = context.getBean(Main.class);
        //Main est une bean Spring
        main.useCase();

    }*/

    public static void main(String[] args) {
        SpringApplication.run(Main.class, args);

    }

    @Bean
    public CommandLineRunner commandLineRunner(ApplicationContext context){
         return args -> useCase();
        }
}
