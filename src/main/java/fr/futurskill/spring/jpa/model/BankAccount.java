package fr.futurskill.spring.jpa.model;

import javax.persistence.*;

@Entity
@Table(name = "bank_account")
public class BankAccount {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(name = "account_number", length = 32, nullable = false, unique = true)
    private String accountNumber;

    @Column(length = 128)
    private String description;

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private BankAccountTransaction lastTransaction;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public BankAccountTransaction getLastTransaction() {
        return lastTransaction;
    }

    public void setLastTransaction(BankAccountTransaction lastTransaction) {
        this.lastTransaction = lastTransaction;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "BankAccount{" +
                "id=" + id +
                ", accountNumber='" + accountNumber + '\'' +
                ", description='" + description + '\'' +
                ", lastTransaction=" + lastTransaction +
                '}';
    }
}
