package fr.futurskill.spring.jpa;

import fr.futurskill.spring.jpa.bo.BankException;
import fr.futurskill.spring.jpa.bo.OperationsBancaire;
import fr.futurskill.spring.jpa.cfg.SpringJpaConfig;
import fr.futurskill.spring.jpa.model.BankAccountTransaction;
import fr.futurskill.spring.jpa.model.CustomerAccount;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.math.BigDecimal;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = SpringJpaConfig.class)
public class SpringDataTest {
    //User Account
    private static final String FIRST_NAME = "Ada";
    private static final String LAST_NAME = "Lovelace";
    private static final String LOGIN = "alovelace";
    private static final String INVALID_LOGIN = "cnorris";
    private static final String BANK_ACCOUNT_NUMBER = "456458-4386-N-1618";

    //Transactions
    private static final BigDecimal AMOUNT = BigDecimal.valueOf(-50);
    private static final String DESCRIPTION = "Achat chez Prallus";

    private static final BigDecimal AMOUNT2 = BigDecimal.valueOf(500);
    private static final String DESCRIPTION2 = "Salaire";

    private static final BigDecimal AMOUNT3 = BigDecimal.valueOf(-30);
    private static final String DESCRIPTION3 = "Ciné";


    @PersistenceContext
    private EntityManager em;

    @Autowired
    private OperationsBancaire operationsBancaire;

    @Test
    public void dummy() {
    }

    //    @Test
    public void testEM() {
        Assert.assertNotNull(em);
        Query query = em.createQuery("SELECT ca FROM CustomerAccount ca WHERE ca.login=:login")
                .setParameter("login", "alovelace");
        List<CustomerAccount> clients = query.getResultList();
    }

    @Test
    public void useCase() throws BankException {
        // 1- Creation d'un compte client
        createCustomerAccount();

        // 2- Ajout d'un compte bancaire
        createBankAccount();

        // 3- Ajout de transactions
        addTransactions();

        // 4 - Affichage de la liste des transaction
        getTransactions();
    }

    private void createCustomerAccount() throws BankException {
        final CustomerAccount customerAccount = new CustomerAccount();

        customerAccount.setFirstName(FIRST_NAME);
        customerAccount.setLastName(LAST_NAME);
        customerAccount.setLogin(LOGIN);

        operationsBancaire.createCustomerAccount(customerAccount);

        // l'object customerAccount a ete modifie, l'id est à 1
        // pour "recreer le meme objet", je repose l'id à 0 -> ce n'est pas dans la bdd
        customerAccount.setId(0);
        try {
            operationsBancaire.createCustomerAccount(customerAccount);
            Assert.fail("Erreur. Une BankException aurait du etre lancee");
        } catch (BankException ex) {
            System.out.println(ex.getMessage());
        }
    }

    private void createBankAccount() throws BankException {
        operationsBancaire.addBankAccount(LOGIN, BANK_ACCOUNT_NUMBER);

        //on recree le meme compte bancaire. Il faut qu'une execpetion Bankexception soit lancée
        try {
            operationsBancaire.addBankAccount(LOGIN, BANK_ACCOUNT_NUMBER);
        } catch (BankException ex) {
            System.out.println(ex.getMessage());
        }
        // On creer un compte bancaire avec un identifiant client qui n'existe pas
        try {
            operationsBancaire.addBankAccount(INVALID_LOGIN, BANK_ACCOUNT_NUMBER);
            Assert.fail("Une exception BankException aurait du être lancee");
        } catch (BankException ex) {
            System.out.println(ex.getMessage());
        }

    }

    private void addTransactions() throws BankException {
        //On ajoute des transaction sur un compte bancaire qui marche
        operationsBancaire.addTransaction(BANK_ACCOUNT_NUMBER, AMOUNT, DESCRIPTION);

        //on test pls transactions
        operationsBancaire.addTransaction(BANK_ACCOUNT_NUMBER, AMOUNT2, DESCRIPTION2);
        operationsBancaire.addTransaction(BANK_ACCOUNT_NUMBER, AMOUNT3, DESCRIPTION3);

    }

    private void getTransactions() throws BankException {
        final List<BankAccountTransaction> transactions = operationsBancaire.getTransactions(BANK_ACCOUNT_NUMBER , 2);
        if (transactions != null){
            for (BankAccountTransaction transaction: transactions){
                System.out.println(transaction);
            }
        }

    }
}
